#/usr/bin/python3
#thing for registering mouse events:
from pynput.mouse import Listener
class mouseGetter():
    def __init__(self, ftc):
        self.ftc = ftc
    def move(self, x, y):
        self.ftc.move(x,y)
    def click(self, x,y,button,pressed):
        self.ftc.click(button, pressed)
    def scroll(self, x,y,dx, dy):
        self.ftc.scroll(dx,dy)
    def start(self):
        print("startMouselisten")
        with Listener(on_move = self.move, on_click = self.click, on_scroll = self.scroll,suppress=True) as listy:
            listy.join()

class ftcPrint():
    def move(self, x,y):
        print("move x:"+str(x)+"|y:"+str(y))
    def scroll(self,dx,dy):
        print("scroll dx:"+str(dx)+"|dy:"+str(dy))
    def click(self,button,pressed):
        print("click button:"+str(button)+"pressed:"+str(pressed))
