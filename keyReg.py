#/usr/bin/python3
#thing for registering keystrokes:
from pynput.keyboard import Listener
class keyGetter():
    def __init__(self, ftc):
        self.ftc = ftc
    def keyPress(self, key):
        self.ftc.key(key,True)
    def keyRelease(self, key):
        self.ftc.key(key,False)
    def start(self):
        print("startKeyListen")
        with Listener(on_press=self.keyPress,on_release = self.keyRelease) as listy:
            listy.join()

class ftcPrint():
    def key(self, Key, pressed):
        print(str(Key)+str(pressed))
