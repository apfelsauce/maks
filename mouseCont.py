#/usr/bin/python3
#thing to controll the mouse
from pynput.mouse import Button, Controller
class mouseSetter():
    def __init__(self):
        self.mouse = Controller()
    def move(self, x,y):
        self.mouse.position = (int(x),int(y))
    def click(self,button,pressed):
        btn = button
        if(button == "Button.right"):
            btn = Button.right
        elif(button == "Button.middle"):
            btn = Button.middle
        elif(button == "Button.left"):
            btn = Button.left
        if(pressed == "True"):
            self.mouse.press(btn)
        else:
            self.mouse.release(btn)
    def scroll(self,dx,dy):
        self.mouse.scroll(int(dx),int(dy))
