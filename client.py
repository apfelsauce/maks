#/usr/bin/python3
#client that receives instructions and makes them happen

import keyCont, mouseCont
import socket, json
from config import clientip

#decodes data and does things with the controllers
def decoder(apfel):
    apfel = apfel.decode("utf-8")
    print(apfel)
    if apfel!="Apfel":
        Json = json.loads(apfel)
        if(Json["type"]=="KEY"):
            hand = keyCont.keySetter()
            print(Json["name"])
            hand.key(Json["name"],Json["state"])
        elif(Json["type"]=="MOVE"):
            mve = mouseCont.mouseSetter()
            mve.move(Json["x"],Json["y"])
        elif(Json["type"]=="SCROLL"):
            mve = mouseCont.mouseSetter()
            mve.scroll(Json["dx"],Json["dy"])
        elif(Json["type"]=="CLICK"):
            mve = mouseCont.mouseSetter()
            mve.click(Json["name"],Json["state"])
def decoder2(apfel):
    print (apfel)

while True:
    try:
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock.bind((clientip,50000))
        sock.listen(1)
        conn, addr = sock.accept()
        #while 1:
        #print("enteringWhile")
        data = conn.recv(1024)
        #print("finished recv")
        #if not data: break
        #print("this is not broken")
        decoder(data)
        #print("done sh*t")
        conn.send("TRUE".encode())
        conn.close()
    except KeyboardInterrupt:
        try:
            conn.close()
        except:
            print("can't connect to the internet!")
        sock.shutdown(socket.SHUT_RDWR)
        sock.close()
        break
    #except OSError:
    #    print("\nOSError")
    #    print("Address in Use or similar")
    #    break
    #except:
    #    try:
    #        conn.close()
    #    except:
    #        print("no connection")
    #    sock.shutdown(socket.SHUT_RDWR)
    #    sock.close()

