#/usr/bin/python3
#thing to controll the keyboard
from pynput.keyboard import Key, Controller, KeyCode
class keySetter():
    def __init__(self):
        self.keyb = Controller()
    def key(self, key, pressed):
        if type(key)is str:
            if(key == "Key.enter"):
                key = Key.enter
            elif(key == "Key.backspace"):
                key = Key.backspace
            elif(key == "Key.shift"):
                key = Key.shift
            elif(key == "Key.shift_r"):
                key = Key.shift_r
            elif(key == "Key.ctrl"):
                key = Key.ctrl
            elif(key == "Key.ctrl_r"):
                key = Key.ctrl_r
            elif(key == "Key.delete"):
                key = Key.delete
            elif(key == "<65027>"):
                key = Key.alt_gr
            elif(key == "Key.tab"):
                key = Key.tab
            elif(key == "Key.num_lock"):
                key = Key.num_lock
            elif(key == "Key.up"):
                key = Key.up
            elif(key == "Key.down"):
                key = Key.down
            elif(key == "Key.left"):
                key = Key.left
            elif(key == "Key.right"):
                key = Key.right
            elif(key == "Key.delete"):
                key = Key.delete
            elif(key == "Key.space"):
                key = Key.space
            else:
                key = key[1]
        if (pressed=="True"):
            self.keyb.press(key)
        else:
            self.keyb.release(key)
#test = keySetter()
#test.key("a","True")
#test.key("Key.num_lock","True")
#test.key("a","False")
#test.key("Key.num_lock","False")
